import java.util.ArrayList;
import java.util.List;

public class Graph {
	List<Node> nodes;
	
	public Graph() {
		nodes = new ArrayList<Node>();
	}
	
	public void addNode(Node node) {
		nodes.add(node);
	}
	
	public Node getNode(String nom) {
		Node aux = null;
		for( Node node : nodes) {
			if (node.getName().equals(nom)) {
				aux = node;
			} 
		}
		return aux;
	}
	
	public String toString() {
		return nodes.toString();
	}
}
