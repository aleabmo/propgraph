import java.util.*;;

public class Node {
	String name;
	List<Edge> connections;
	boolean visitat = false;
	public Node(String name) {
		this.name = name;
		this.connections = new ArrayList<Edge>();
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return this.name;
	}
	
	public void addEdge(Edge edge) {
		this.connections.add(edge);
	}
	
	//Funcio que retorna els fill
	public List<Node> getFills(){
		List<Node> aux = new ArrayList<Node>();
		for (Edge edge : connections) {
			if (!edge.end.visitat) aux.add(edge.end);
			if (!edge.start.visitat) aux.add(edge.start);
		}
		return aux;
	}
	
	//Funcio que retorna els fill ordenats per weight
	public List<Node> getFillsSort(){
		List<Node> aux = new ArrayList<Node>();
		connections.sort(null);
		for (Edge edge : connections) {
			if (!edge.end.visitat) aux.add(edge.end);
			if (!edge.start.visitat) aux.add(edge.start);
		}
		return aux;
	}
}
