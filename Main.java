
public class Main {
	public static void main(String[] args) {
		
       Func func = new Func();
     
       //Definim nodes i Arestes de les transparencies
       
       Node n1 = new Node("A");
       Node n2 = new Node("B");
       Node n3 = new Node("C");
       Node n4 = new Node("D");
       Node n5 = new Node("E");
       Node n6 = new Node("F");
       Node n7 = new Node("G");
       Node n8 = new Node("S");
       
       //Node A
       Edge e1 = new Edge(n1,n2, 4);
       Edge e2 = new Edge(n1,n4, 5);
       Edge e3 = new Edge(n1,n8, 3);
       
       //Node B
       Edge e4 = new Edge(n2,n3, 4);
       Edge e5 = new Edge(n2,n5, 5);
       Edge e6 = new Edge(n2,n1, 4);
       
       //Node C
       Edge e7 = new Edge(n3,n2, 4);
       
       //Node D
       Edge e8 = new Edge(n4,n1, 5);
       Edge e9 = new Edge(n4,n5, 2);
       Edge e10 = new Edge(n4,n8, 4);
       
       //Node E
       Edge e11 = new Edge(n5,n2, 5);
       Edge e12 = new Edge(n5,n4, 2);
       Edge e13 = new Edge(n5,n6, 4);
       
       //Node F
       Edge e14 = new Edge(n6,n5, 4);
       Edge e15 = new Edge(n6,n7, 3);
       
       //Node G
       Edge e16 = new Edge(n7,n6, 3);
       
       //Node S
       Edge e17 = new Edge(n8,n1, 3);
       Edge e18 = new Edge(n8,n4, 4);
       
       n1.addEdge(e1);
       n1.addEdge(e2);
       n1.addEdge(e3);
       
       n2.addEdge(e4);
       n2.addEdge(e5);
       n2.addEdge(e6);
       
       n3.addEdge(e7);
       
       n4.addEdge(e8);
       n4.addEdge(e9);
       n4.addEdge(e10);
       
       n5.addEdge(e11);
       n5.addEdge(e12);
       n5.addEdge(e13);
       
       n6.addEdge(e14);
       n6.addEdge(e15);
       
       n7.addEdge(e16);
       
       n8.addEdge(e17);
       n8.addEdge(e18);
       
       //Afegim els nodes al graph
       Graph graf1 = new Graph();
       graf1.addNode(n1);
       graf1.addNode(n2);
       graf1.addNode(n3);
       graf1.addNode(n4);
       graf1.addNode(n5);
       graf1.addNode(n6);
       graf1.addNode(n7);
       graf1.addNode(n8);
       
       //Cridem a la funció, partint del node 8 <=> Node S
       func.ampladaGraph(graf1, n8.getName());
       
       //Resetegem el Visitat/no visitat dels nodes
       for (Node node : graf1.nodes) {
    	   node.visitat=false;
       }
       
       //Cridem a la funció, partint del node 8 <=> Node S
       func.profunditatGraph(graf1, n8.getName());
       
       for (Node node : graf1.nodes) {
    	   node.visitat=false;
       }
       
       //Cridem a la funció, partint del node 8 <=> Node S
       func.constantGraph(graf1, n8.getName());
    }
	
}
