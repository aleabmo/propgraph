import java.util.ArrayList;

public class Cua<T> {
	
	ArrayList<T> elems;
	
	public Cua() {
		elems = new ArrayList<T>();
	}
	public void addElem(T elem) {
		elems.add(elem);
	}
	
	public T getElem(){
		T elem = elems.get(0);
		elems.remove(0);
		return elem;
	}
	public T getElem(int index){
		T elem = elems.get(index);
		return elem;
	}
	
	public boolean isEmpty() {
		return elems.isEmpty();
	}

	
}
