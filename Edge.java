
public class Edge implements Comparable<Object>{
	Node start;
	Node end;
	int weight;
	
	public Edge (Node start, Node end, int weight) {
		this.start = start;
		this.end = end;
		this.weight = weight;
	}
	
	//Implementem el compareTo per tal de poder ordenar la llista.
	public int compareTo(Object anotherEdge) throws ClassCastException {
		if(!(anotherEdge instanceof Edge))
			throw new ClassCastException("No es un Edge");
		int anotherWeight = ((Edge) anotherEdge).weight
				;
		return anotherWeight - this.weight;
		
	}
}
