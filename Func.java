
public class Func {

	public void ampladaGraph(Graph graf, String start) {
		System.out.println("##### AMPLADA #####");
		Cua<Node> nvisitar = new Cua<Node>();
		boolean solucio =  false;
		Graph arbre = new Graph();

		nvisitar.addElem(graf.getNode(start));
		
		while( !nvisitar.isEmpty() && !solucio ){
			Node cas = nvisitar.getElem();
			cas.visitat = true;
			if ( !isSolucio(cas)) {
				arbre.addNode(cas);
				for ( Node fill : cas.getFills()) {
					nvisitar.addElem(fill);
				}
				System.out.println("Arbre: "+ arbre);
			} else solucio = true;
		}
		
		
	}
	
	public boolean isSolucio(Node node){
		if ( node.getName().equals("G"))
			return true;
		else return false;
	}
	
	public void profunditatGraph(Graph graf, String start) {
		System.out.println("##### PROFUNDITAT #####");
		Pila<Node> nvisitar = new Pila<Node>();
		boolean solucio =  false;
		Graph arbre = new Graph();

		nvisitar.addElem(graf.getNode(start));
		
		while( !nvisitar.isEmpty() && !solucio ){
			Node cas = nvisitar.getElem();
			cas.visitat = true;
			if ( !isSolucio(cas)) {
				arbre.addNode(cas);
				for ( Node fill : cas.getFills()) {
					nvisitar.addElem(fill);
				}
				System.out.println("Nivell: "+ arbre);
			} else solucio = true;
		}
		
		
	}
	
	public void constantGraph(Graph graf, String start) {
		System.out.println("##### CONSTANT #####");
		Pila <Node> nvisitar = new Pila<Node>();
		boolean solucio =  false;
		Graph arbre = new Graph();

		nvisitar.addElem(graf.getNode(start));
		
		while( !nvisitar.isEmpty() && !solucio ){
			Node cas = nvisitar.getElem();
			cas.visitat = true;
			if ( !isSolucio(cas)) {
				arbre.addNode(cas);
				for ( Node fill : cas.getFillsSort()) {
					nvisitar.addElem(fill);
				}
				System.out.println("Nivell: "+ arbre);
			} else solucio = true;
		}
	}
}
