import java.util.LinkedList;

public class Pila<T> {

	LinkedList<T> elems;
	public Pila() {
		elems = new LinkedList<T>();
	}
	public void addElem(T elem) {
		elems.add(elem);
	}
	public T getElem(){
		T elem = elems.getLast();
		elems.removeLast();
		return elem;
	}
	public boolean isEmpty() {
		return this.elems.isEmpty();
	}
}
